﻿using Cyotek.Collections.Generic;
using MonoGame.Extended;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Particles {
    public class WiggleParticle<T> : FunctionalParticleBase<T, float> {
        protected FastRandom random;

        public WiggleParticle(int x, int y, int seed) {
            this.X = x;
            this.Y = y;

            MovementFunctionPositiveX = (t) => 0;
            MovementFunctionPositiveY = (t) => 0;

            MovementFunctionNegativeX = (t) => 0;
            MovementFunctionNegativeY = (t) => 0;
            random = new FastRandom(seed);
        }

        public override void Step() {
            X += checkFunc(MovementFunctionPositiveX);
            Y += checkFunc(MovementFunctionPositiveY);

            X -= checkFunc(MovementFunctionNegativeX);
            Y -= checkFunc(MovementFunctionNegativeY);
        }

        private int checkFunc(Func<int, float> func) {
            return random.NextSingle() < MathF.Abs(func(Age)) ? MathF.Sign(func(Age)) : 0;
        }
    }
}
