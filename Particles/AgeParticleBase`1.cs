﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Particles {
    public abstract class AgeParticleBase<TData> : IParticle<TData>, IEnumerable<(int x, int y, int t, TData d)> {
        public int X { get; set; }
        public int Y { get; set; }

        public int Age { get; private set; }

        public Func<int, TData> DataFunction { get; set; }
        public TData Data => DataFunction(Age);

        private Cacher<(int, int, int, TData)> cache = new Cacher<(int, int, int, TData)>(360);

        public void PreStep() {
            cache.Cache((X, Y, Age, Data));
        }

        public abstract void Step();

        public void PostStep() { Age++; }

        public IEnumerator<(int x, int y, int t, TData d)> GetEnumerator() {
            return cache.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return (IEnumerator)cache.GetEnumerator();
        }
    }
}
