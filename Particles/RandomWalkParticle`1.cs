﻿using MonoGame.Extended;
using System;
using System.Collections;
using System.Linq;

namespace Particles {
    public class RandomWalkParticle<TData> : AgeParticleBase<TData> {
        FastRandom random;

        public RandomWalkParticle(int x, int y, FastRandom random = null) {
            this.X = x;
            this.Y = y;

            if (random == null) {
                this.random = new FastRandom();
            } else {
                this.random = random;
            }
        }

        public override void Step() {
            this.X += random.Next(-1, 1);
            this.Y += random.Next(-1, 1);
        }
    }
}
