﻿using Cyotek.Collections.Generic;
using MathNet.Numerics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.ViewportAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.MathF;

namespace Particles {
    public class Game1 : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ParticleManager<HslColor> particleManager;
        int frameNumber;
        Camera2D camera;
        Random globalRandom = new Random();

        bool running = false;
        KeyboardState currentKeyboardState;

        RenderTarget2D image;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            particleManager = new ParticleManager<HslColor>(globalRandom.Next() + 1);

            IsFixedTimeStep = false;

            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1440;
        }

        protected override void Initialize() {
            base.Initialize();
            
            //var viewportAdapter = new BoxingViewportAdapter(Window, GraphicsDevice, 1440, 360);
            camera = new Camera2D(GraphicsDevice);

            image = new RenderTarget2D(GraphicsDevice,
                GraphicsDevice.PresentationParameters.BackBufferWidth, GraphicsDevice.PresentationParameters.BackBufferHeight,
                false, GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime) {
            KeyboardState previousKeyboardState = this.currentKeyboardState;
            this.currentKeyboardState = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || currentKeyboardState.IsKeyDown(Keys.Escape))
                Exit();

            if (running) {

                if (frameNumber < 1) {
                    for (int i = -720; i < 1280; i++) {
                    //var i = 1;
                        var offset = i;
                        var newParticle = particleManager.AddWiggleParticle(0, 0 + i, (t) => new HslColor(offset/10f * 10, 1f, .5f));
                        newParticle.MovementFunctionPositiveY = (t) => Sin(t.ToRadians()) / 2f - Cos(t.ToRadians()) / 2f;
                        newParticle.MovementFunctionPositiveX = (t) => Pow(Cos((t/4f).ToRadians() + PI), 2);
                    }
                }

                for (int i = 0; i < 5; i++) {
                    particleManager.Step();
                    particleManager.Cull();
                    RenderScene();
                }
                frameNumber++;
            }

            if (currentKeyboardState.IsKeyDown(Keys.Up)) {
                camera.Move(-Vector2.UnitY * 4);
            } else if (currentKeyboardState.IsKeyDown(Keys.Down)) {
                camera.Move(Vector2.UnitY * 4);
            } else if (currentKeyboardState.IsKeyDown(Keys.Left)) {
                camera.Move(-Vector2.UnitX * 4);
            } else if (currentKeyboardState.IsKeyDown(Keys.Right)) {
                camera.Move(Vector2.UnitX * 4);
            } else if (currentKeyboardState.IsKeyDown(Keys.PageDown)) {
                camera.ZoomIn(.02f);
            } else if (currentKeyboardState.IsKeyDown(Keys.PageUp)) {
                camera.ZoomOut(.02f);
            } else if (currentKeyboardState.IsKeyDown(Keys.Space) && !previousKeyboardState.IsKeyDown(Keys.Space)) {
                running = !running;
            } else if (currentKeyboardState.IsKeyDown(Keys.S) && !previousKeyboardState.IsKeyDown(Keys.S)) {
                RenderScene();
                using (var fs = new FileStream(DateTime.Now.ToString("yyyy-MM-dd hh.mm.ss") + ".png", FileMode.Create)) {
                    image.SaveAsPng(fs, image.Width, image.Height);
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            RenderScene();

            spriteBatch.Begin();
            spriteBatch.Draw(image, Vector2.Zero, Color.White);
            spriteBatch.End();
            
            base.Draw(gameTime);
        }

        private void RenderScene() {
            GraphicsDevice.SetRenderTarget(image);
            spriteBatch.Begin(transformMatrix: camera.GetViewMatrix());

            foreach (var p in particleManager.Cast<AgeParticleBase<HslColor>>()) {
                //foreach ((int x, int y, int t, HslColor hsl) in p) {
                //    var newhsl = new HslColor(hsl.H, hsl.S, hsl.L);
                //    spriteBatch.DrawPoint(x, y, newhsl.ToRgb(), 3);
                //}

                spriteBatch.DrawPoint(p.X, p.Y, p.Data.ToRgb(), 5);
            }

            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
        }
    }
}
