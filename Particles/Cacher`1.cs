﻿using Cyotek.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Particles {
    public class Cacher<TData> : IEnumerable<TData> {
        private CircularBuffer<TData> buffer;

        public Cacher(int cacheSize) {
            buffer = new CircularBuffer<TData>(cacheSize);
        }

        public void Cache(TData item) => buffer.Put(item);

        public IEnumerator<TData> GetEnumerator() {
            return buffer.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return (IEnumerator)buffer.GetEnumerator();
        }
    }
}
