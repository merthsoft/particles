﻿using Cyotek.Collections.Generic;
using MonoGame.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Particles {
    public class AgeParticle<T> : FunctionalParticleBase<T, int> {
        public AgeParticle(int x, int y) {
            X = x;
            Y = y;
            MovementFunctionPositiveX = (t) => 0;
            MovementFunctionPositiveY = (t) => 0;
            MovementFunctionNegativeX = (t) => 0;
            MovementFunctionNegativeY = (t) => 0;
        }

        public override void Step() {
            X += MovementFunctionPositiveX(Age);
            Y += MovementFunctionPositiveY(Age);

            X -= MovementFunctionNegativeX(Age);
            Y -= MovementFunctionNegativeY(Age);
        }
    }
}
