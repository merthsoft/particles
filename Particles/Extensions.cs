﻿using Microsoft.Xna.Framework;
using System;
using System.Numerics;

namespace Particles {
    static class Extensions {
        public static float Map(this double oldValue, double newMin, double newMax, double oldMin = 0f, double oldMax = 1f) {
            return Map((float)oldValue, (float)newMin, (float)newMax, (float)oldMin, (float)oldMax);
        }

        public static float Map(this double oldValue, float newMin, float newMax, float oldMin = 0f, float oldMax = 1f) {
            return Map((float)oldValue, newMin, newMax, oldMin, oldMax);
        }

        public static float Map(this float oldValue, float newMin, float newMax, float oldMin = 0f, float oldMax = 1f) {
            var OldRange = (oldMax - oldMin);
            var NewValue = 0f;
            if (OldRange == 0) {
                NewValue = newMin;
            } else {
                var NewRange = (newMax - newMin);
                NewValue = (((oldValue - oldMin) * NewRange) / OldRange) + newMin;
            }

            return NewValue;
        }

        public static float ToDegrees(this float val) {
            return (float)(180.0 / Math.PI) * val;
        }

        public static float ToDegrees(this double val) {
            return ToDegrees((float)val);
        }
        public static float ToRadians(this float val) {
            return (float)(Math.PI / 180.0) * val;
        }

        public static float ToRadians(this double val) {
            return ToRadians((float)val);
        }

        public static float ToRadians(this BigInteger val) {
            return ToRadians((float)val);
        }

        public static float ToRadians(this int val) {
            return ToRadians((float)val);
        }

        public static Color GetGradientBetweenAtStep(this Color start, Color end, int steps, int step) {
            int stepA = ((end.A - start.A) / (steps - 1));
            int stepR = ((end.R - start.R) / (steps - 1));
            int stepG = ((end.G - start.G) / (steps - 1));
            int stepB = ((end.B - start.B) / (steps - 1));
           
            return new Color(start.R + (stepR * step), start.G + (stepG * step), start.B + (stepB * step), start.A + (stepA * step));
        }
    }
}
