﻿using Cyotek.Collections.Generic;
using MonoGame.Extended;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Particles {
    public class ParticleManager<TData> : IEnumerable<IParticle<TData>> {
        private List<IParticle<TData>> particles { get; set; } = new List<IParticle<TData>>();
        int seed;

        public ParticleManager(int seed) {
            this.seed = seed;
        }

        public void Step() {
            foreach (var particle in particles) {
                particle.PreStep();
                (var prevX, var prevY) = (particle.X, particle.Y);
                particle.Step();
                //if (particles.Exists(p => p != particle && p.X == particle.X && p.Y == particle.Y)) {
                //    particle.X = prevX;
                //    particle.Y = prevY;
                //}
                particle.PostStep();
            }
        }

        public void Cull() {
            //particles.RemoveAll(p => p.X >= 1200);
            foreach (var particle in particles) {
                if (particle.X >= 1440) { particle.X = 0; }
                else if (particle.X <= 0) { particle.X = 1439; }
                //if (particle.Y >= 360) { particle.Y = 0; }
                //else if (particle.Y < 0) { particle.Y = 359; }
            }
        }

        public WiggleParticle<TData> AddWiggleParticle(int x, int y, Func<int, TData> dataFunc) {
            seed = new Random().Next();
            WiggleParticle<TData> item = new WiggleParticle<TData>(x, y, seed) { DataFunction = dataFunc };
            particles.Add(item);
            return item;
        }

        public void AddParticle(IParticle<TData> particle) {
            particles.Add(particle);
        }

        public IEnumerator<IParticle<TData>> GetEnumerator() {
            return particles.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return particles.GetEnumerator();
        }
    }
}
