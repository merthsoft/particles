﻿using System;
using System.Collections.Generic;

namespace Particles {
    public interface IParticle<TData> : IStepper {
        int X { get; set; }
        int Y { get; set; }

        TData Data { get; }
    }
}
