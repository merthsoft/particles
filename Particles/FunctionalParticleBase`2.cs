﻿using Cyotek.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Particles {
    public abstract class FunctionalParticleBase<TData, TFuncParam> : AgeParticleBase<TData>{
        public Func<int, TFuncParam> MovementFunctionPositiveX { get; set; }
        public Func<int, TFuncParam> MovementFunctionNegativeX { get; set; }

        public Func<int, TFuncParam> MovementFunctionPositiveY { get; set; }
        public Func<int, TFuncParam> MovementFunctionNegativeY { get; set; }
    }
}
