﻿using System;
using System.Linq;

namespace Particles {
    public struct Direction {
        public static Direction Null = new Direction(0, 0);
        public static Direction North = new Direction(0, -1);
        public static Direction NorthEast = new Direction(1, -1);
        public static Direction East = new Direction(1, 0);
        public static Direction SouthEast = new Direction(1, 1);
        public static Direction South = new Direction(0, 1);
        public static Direction SouthWest = new Direction(-1, 1);
        public static Direction West = new Direction(-1, 0);
        public static Direction NorthWest = new Direction(-1, -1);

        public static Direction Unit = new Direction(1, 1);
        public static Direction FlipX = new Direction(1, -1);
        public static Direction FlipY = new Direction(-1, 1);
        public static Direction FlipXY = new Direction(-1, -1);

        public (int, int) Value { get; private set; }

        public int A => Value.Item1;
        public int B => Value.Item2;

        public Direction((int, int) a) : this(a.Item1, a.Item2) { }
        public Direction(int a, int b) {
            //if (a > 1 || a < -1) { throw new ArgumentOutOfRangeException("a", a, "Values but be -1, 0, 1."); }
            //if (b > 1 || b < -1) { throw new ArgumentOutOfRangeException("b", b, "Values but be -1, 0, 1."); }
            if (a > 1) { a = 1; }
            if (a < -1) { a = -1; }
            if (b > 1) { b = 1; }
            if (b < -1) { b = -1; }

            Value = (a, b);
        }

        public void Deconstruct(out int a, out int b) { a = Value.Item1; b = Value.Item2; }

        public static implicit operator (int, int) (Direction d) => d.Value;
        public static implicit operator Direction((int, int) a) => new Direction(a);
        public static (int, int) operator +((int, int) a, Direction d) => (a.Item1 + d.A, a.Item2 + d.B);

        public static Direction operator +(Direction a, Direction b) {
            (var newA, var newB) = (a.A + b.A, a.B + b.B);
            if (newA == -2) { newA = +1; }
            if (newA == +2) { newA = -1; }
            if (newB == -2) { newB = +1; }
            if (newB == +2) { newB = -1; }

            return (newA, newB);
        }

        public static Direction operator *(Direction a, Direction b) {
            return new Direction(a.A * b.A, a.B * b.B);
        }

        public Direction RotatePositive90() {
            return new Direction(-B, A);
        }

        public Direction RotateNegative90() {
            return new Direction(B, A);
        }
    }
}
