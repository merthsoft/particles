﻿using System;

namespace Particles {
    public interface IStepper {
        int Age { get; }
        void PreStep();
        void Step();
        void PostStep();
    }
}
